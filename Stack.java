/* Java program to implement basic stack 
operations */
class Stack { 
	static final int MAX_ITEMS = 5; 
	int top; 
	String items[] = new String[MAX_ITEMS]; // Maximum size of Stack 

	public Stack() {
		top = 0;
	}
	
	public boolean isEmpty() {
		if (top == 0) {
			return true;
		}
		return false;
	}
	
	public boolean isFull() {
		if (top == MAX_ITEMS) {
			return true;
		}
		return false;
	}
	
	public boolean push(String item) {
		if (top >= (MAX_ITEMS - 1)) { 
			System.out.println("Maximum capacity reached.");
			return false;
		} 
		else { 
			items[top] = item;
			top++;
			System.out.println(item + " pushed into stack"); 
			return true; 
		} 
	}
 
	public String pop() throws Exception {
		if (top < 0) { 
			throw new StackException("Stack underflow error."); 
		} 
		else {
			String item = items[top];
			top--;
			return item; 
		} 
	}

	public String peek() {
		return items[top];
	}
	
	public int size() {
		return top;
	}
	
	public void search() throws Exception {
		throw new StackException("This function is not implemented");
	}
	
} 



