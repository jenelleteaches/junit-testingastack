# Custom implementation of a Stack data structure

## Requirements

* A stack is always LIFO (last in, first out)
* The code must keep track of the "top" of the the stack

## Functions

* push() --> add item to the top of the stack
* pop() --> remove the top item from the stack
* peek() --> look at item but do not delete it

* isFull() --> check if stack is at max capacity
* isEmpty() --> check if stack is empty
* size() --> returns total number of items in stack

* search() --> search the stack for an item
* print() --> show all contents of the stack

## Question

The stack implementation contains errors.

Write JUnit test to test the Stack. 

You do *not* need to FIX the errors
You just need to write test cases!

## Resources on Stacks

https://www.youtube.com/watch?v=F1F2imiOJfk

